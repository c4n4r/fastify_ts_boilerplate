import { FastifyInstance, RouteOptions } from "fastify";

export default interface RouterInterface {
    defineRoutes(): FastifyInstance;
    getAvailableRoutes(): RouteOptions[]
}