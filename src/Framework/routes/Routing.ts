import { FastifyInstance, FastifySchema, RouteOptions } from "fastify";
import { RouteGenericInterface } from "fastify/types/route";
import { Server, IncomingMessage, ServerResponse } from "http";
import RouterInterface from "../RouterInterface";
import {routes} from './RoutesDefinition';
export default class Routing implements RouterInterface{
    

    constructor(private server: FastifyInstance){}


    private get routes():RouteOptions[] {
        return routes;
    }

    
    getAvailableRoutes(): RouteOptions<Server, IncomingMessage, ServerResponse, RouteGenericInterface, unknown, FastifySchema>[] {
        return this.routes;
    }
    defineRoutes(): FastifyInstance {
       this.routes.map((route: RouteOptions) => {
           this.server.route(route);
       })
       return this.server;
    }

}