import { FastifyReply, FastifyRequest, RouteOptions } from "fastify"

export const routes: RouteOptions[] = [
    {
        method: 'GET',
        url: '/',
        schema: {
            querystring: {
                name: { type: 'string' }
            },
            response: {
                200: {
                    type: 'object',
                    properties: {
                        test: { type: 'string' }
                    }
                }
            }
        },
        handler: async (request: FastifyRequest, reply: FastifyReply) => {
            return { test: 'oui' }
        }
    }
]