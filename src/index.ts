import fastify, { FastifyInstance, FastifyReply, FastifyRequest, RequestGenericInterface } from "fastify";
import Routing from "./Framework/routes/Routing";

let server: FastifyInstance = fastify({ logger: true });
//start routing
const router = new Routing(server);
server = router.defineRoutes();
// Run the server!
const start = async () => {
    try {
        await server.listen(3000)
    } catch (err) {
        server.log.error(err)
        process.exit(1)
    }
}
start()