import fastify, { FastifyInstance, FastifyReply, FastifyRequest, RouteOptions } from "fastify"
import Routing from "../src/Framework/routes/Routing";

let server: FastifyInstance = fastify({});

const router = new Routing(server);

test('should return a instance of Fastify', () => {
    server = router.defineRoutes();
    expect(server.listen).toBeDefined;
})

test('definitions should be an array', () => {
    const routes = router.getAvailableRoutes(); 
    routes.map(route => {
        expect(route).toHaveProperty('url');
        expect(route).toHaveProperty('method');
        expect(route).toHaveProperty('handler');
        
    })
}) 